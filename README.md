# Text Messages: Spam / Ham (Gaussian NB)

**By: Andrew Wairegi**

## Description
To identify whether a text message is spam / not. This will be done using word
counts. Which will be gotten by a count vectorizer. Then converted to a Tdfif document.
Which will then be fed into our naive bayes model. This is the aim of the project.
I want to make a text spam/ham model. That will allow us to predict spam text messages
with an accuracy of atleast 70%.

[Open notebook]

## Setup/Installation instructions
1. Create a folder on your computer
2. Set it up as a repository (using git init)
3. Clone this repository there (using git clone https://...)
4. Upload the notebook to a google drive folder
5. Open it
6. Upload the data files for the collab there in the notebook (using the file upload section)
7. Run the notebook

## Known Bugs
There are no known issues / bugs.

## Technologies Used
1. Python - The programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data Analysis Package
4. Seaborn - A Visualization package
5. Matplotlib - A Visualization package
6. Scikit learn - A Modelling package

<br>

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/machine-learning/GaussianNB-Text_messages_spam_vs_ham/-/blob/main/Text_messages_spam_vs_ham_(gaussian_NB).ipynb
